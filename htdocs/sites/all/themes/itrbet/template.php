<?php

/**
 * Implements hook_preprocess_html().
 */
function itrbet_preprocess_html(&$vars) {
  $vars['html_attributes_array'] = array();
  $vars['body_attributes_array'] = array();
  // HTML element attributes.
  $vars['html_attributes_array']['lang'] = $vars['language']->language;
  $vars['html_attributes_array']['dir'] = $vars['language']->dir;
  // Adds RDF namespace prefix bindings in the form of an RDFa 1.1 prefix
  // attribute inside the html element.
  if (function_exists('rdf_get_namespaces')) {
    $vars['rdf'] = new stdClass;
    foreach (rdf_get_namespaces() as $prefix => $uri) {
      if (property_exists($vars['rdf'], 'prefix')) {
        $vars['rdf']->prefix .= $prefix . ': ' . $uri . "\n";
      }
    }
    if (property_exists($vars['rdf'], 'prefix')) {
      $vars['html_attributes_array']['prefix'] = $vars['rdf']->prefix;
    }
  }
  // Body element attributes.
  $vars['body_attributes_array']['class'] = $vars['classes_array'];
  $vars['body_attributes_array'] += $vars['attributes_array'];
  $vars['attributes_array'] = '';

  if (in_array(current_path(), ['user', 'user/register', 'user/password'])) {
    $vars['body_attributes_array']['class'][] = 'is_user';
  }
}

/**
 * Implements hook_process_html().
 */
function itrbet_process_html(&$vars) {
  // Flatten out html_attributes and body_attributes.
  $vars['html_attributes'] = drupal_attributes($vars['html_attributes_array']);
  $vars['body_attributes'] = drupal_attributes($vars['body_attributes_array']);
}

/**
 * Implements hook_preprocess_page().
 */
function itrbet_preprocess_page(&$variables) {
  if (in_array(current_path(), ['user', 'user/register', 'user/password'])) {
    $variables['is_user'] = TRUE;
  } else {
    $variables['is_user'] = FALSE;
  }
}

/**
 * Implements hook_html_head_alter().
 */
function itrbet_html_head_alter(&$head_elements) {
  // Simplify the meta charset declaration.
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8',
  );
}

/**
 * Implements theme_menu_tree().
 */
function itrbet_menu_tree($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}