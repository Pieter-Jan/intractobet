<?php if (!$is_user): ?>
<!-- HEADER -->
<header>
    <div class="logo">
        <a href="<?php print url(NULL, ['absolute' => TRUE]); ?>">
            <img src="/sites/all/themes/itrbet/img/logo.png" alt="<?php print variable_get('site_name'); ?>" title="<?php print variable_get('site_name'); ?>"/>
        </a>
    </div>
    <!-- MENU -->
    <nav>
      <?php print render($page['menu']); ?>
    </nav>
</header>
<?php endif; ?>

<!-- CONTENT -->
<main class="<?php $is_user ? print 'is_user' : NULL; ?>">
  <?php if ($messages): ?>
    <?php print $messages; ?>
  <?php endif; ?>

  <?php print render($tabs); ?>
  <?php if ($is_user): ?>
    <div class="user-wrapper">
      <div class="logo">
        <img src="/sites/all/themes/itrbet/img/logo.png" alt="<?php print variable_get('site_name'); ?>" title="<?php print variable_get('site_name'); ?>"/>
      </div>
  <?php endif; ?>
  <?php print render($page['content']); ?>
  <?php if ($is_user): ?>
    </div>
  <?php endif; ?>
  <?php if (!$is_user): ?>
    <?php print render($page['content_bottom']); ?>
  <?php endif; ?>
</main>

<?php if (!$is_user): ?>
<!-- FOOTER -->
<footer>
    <div class="footer footer-left">
        &copy; <?php print date('Y') ?> iO
    </div>
    <div class="footer footer-right">
    </div>
</footer>
<?php endif; ?>