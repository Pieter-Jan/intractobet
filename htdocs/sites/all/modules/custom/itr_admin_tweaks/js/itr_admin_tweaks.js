/**
 * @file
 * Attaches behaviors for the Contextual module.
 */

(function ($) {

    Drupal.itrAdminTweaks = Drupal.itrAdminTweaks || {};


    $(document).ready(function () {
        // Bind window resize event
        $(window).resize(function () {
            Drupal.itrAdminTweaks.updateAdminHeight();
        });

        // Bind change tab admin toolbar
        $('#admin-toolbar .admin-tabs').click(function () {
            Drupal.itrAdminTweaks.updateAdminHeight();

        })

        // Initial update on page load
        Drupal.itrAdminTweaks.updateAdminHeight();

    });


    // Fix voor teveel items in admin, hide itr banner
    Drupal.itrAdminTweaks.updateAdminHeight = function () {

        window_height = $(window).height();

        admin_menu = $("#admin-menu").height();
        admin_tabs = $("#admin-toolbar .admin-tabs").height();

        $("#admin-toolbar .admin-active ul.menu").height('auto'); // Reset before measuring
        admin_activeblock = $("#admin-toolbar .admin-active ul.menu").height();

        height_left_for_itrbanner = window_height - (admin_menu + admin_tabs + admin_activeblock);

        if (height_left_for_itrbanner < 220) {
            $('#itr-support-block').hide();
        } else {
            $('#itr-support-block').show();
        }

        // Resize visible menu links
        $("#admin-toolbar .admin-active .block-content").height(window_height - 57 - (admin_menu + admin_tabs));

    }

})(jQuery);
