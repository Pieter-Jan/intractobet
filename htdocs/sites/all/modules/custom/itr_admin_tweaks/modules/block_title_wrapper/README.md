# Block Title Wrapper
## What it is
This module provides a new field on the block configuration page. Using this field you can define any HTML-tag you want to use as a wrapper for the block title.

Some specifications:

- This setting is not exportable via features.
- If you need a custom block template file in your theme, copy the one from this module instead of the core block.tpl.php file.
- Any issues in the block_class module will be relevant to this module.


## Usage
- Enable the block_title_wrapper module.
- Go to configuration page of a block of your choice.
- Enter an HTML-tag in the wrapper field. Attention, enter only the tag name without <>.

    Example: If you want to change the HTML-tag to a <span>-tag enter "span".

## Future plans
None at the moment.
You are free to send any feedback and suggestions. I'd be glad to make improvements.

## Module Dependencies
None, except for the core block module.

## Author information
Module developed by Alvin Coessens, based on the block_class module.

