<?php

/**
 * @file
 * Install, update and uninstall functions for the block_title_wrapper module.
 */

/**
 * Implements hook_install().
 */
function block_title_wrapper_install() {
  $schema['block'] = array();
  block_title_wrapper_schema_alter($schema);
  foreach ($schema['block']['fields'] as $field => $spec) {
    if (db_field_exists('block', $field)) {
      watchdog('system', 'Module install: Attempt to recreate field: "%field", when it already exists.', array('%field' => $field), WATCHDOG_WARNING);
    }
    else {
      db_add_field('block', $field, $spec);
    }
  }

  // Execute as almost-final module to prevent that
  // the theme registry is overwritten by other modules.
  db_query("UPDATE {system} SET weight = 99998 WHERE name = 'block_title_wrapper'");
}

/**
 * Implements hook_uninstall().
 */
function block_title_wrapper_uninstall() {
  $schema['block'] = array();
  block_title_wrapper_schema_alter($schema);
  foreach ($schema['block']['fields'] as $field => $specs) {
    db_drop_field('block', $field);
  }
}

/**
 * Implements hook_schema_alter().
 *
 * Other modules, such as i18n_block also modify the block database table.
 */
function block_title_wrapper_schema_alter(&$schema) {
  if (isset($schema['block'])) {
    $schema['block']['fields']['title_tag'] = array(
      'type' => 'varchar',
      'length' => 10,
      'not null' => TRUE,
      'default' => '',
      'description' => 'String containing the title wrapper for the block.',
    );
  }
}
