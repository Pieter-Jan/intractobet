<div class="detail-wrapper">
    <div class="match-wrapper">
        <div class="home <?php $match->score_home > $match->score_away ? print 'win' : NULL; ?>">
            <div class="team"><?php print _itr_bet_get_term_name($match->home); ?></div>
            <div class="logo"><?php print (_itr_get_term_icon($match->home) ? '<img src="' . _itr_get_term_icon($match->home) . '" />' : ''); ?></div>
            <div class="score"><?php print ($match->score_home != '' ? $match->score_home : '-'); ?></div>
        </div>
        <div class="away" <?php $match->score_home < $match->score_away ? print 'win' : NULL; ?>>
            <div class="score"><?php print ($match->score_away != '' ? $match->score_away : '-'); ?></div>
            <div class="logo"><?php print (_itr_get_term_icon($match->away) ? '<img src="' . _itr_get_term_icon($match->away) . '" />' : ''); ?></div>
            <div class="team"><?php print _itr_bet_get_term_name($match->away); ?></div>
        </div>
    </div>
    <div class="users-wrapper">
      <?php foreach ($rows as $row): ?>
        <?php // Set class for correct/exact ?>
        <?php $class =
          ($match->score_home != '' && $match->score_away != '') && (
          ($match->score_home == $match->score_away && $row->home == $row->away) ||
          ($match->score_home < $match->score_away && $row->home < $row->away) ||
          ($match->score_home > $match->score_away && $row->home > $row->away)) ? 'correct' : ''; ?>
        <?php $class = $match->score_home == $row->home && $match->score_away == $row->away ? 'exact' : $class; ?>
        <?php $class = isset($match->score_home, $match->score_away) && empty($class) ? 'wrong' : $class; ?>
          <div class="user-wraper <?php print $class; ?>">
              <div class="user"><?php print $row->name; ?></div>
              <div class="user-bet">
                  <span><?php $row->home == '' ? print '/' : print $row->home; ?></span><span>&nbsp;-&nbsp;</span><span><?php $row->away == '' ? print '/' : print $row->away; ?></span>
              </div>
          </div>
      <?php endforeach; ?>
    </div>
    <div class="detail-footer">
        <a href="#" onclick="window.history.back(); return false;"><?php print t('Back'); ?></a>
    </div>
</div>