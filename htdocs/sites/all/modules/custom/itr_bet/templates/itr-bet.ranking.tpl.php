<div class="ranking-header">
    <label for="comp"><?php print t('Competition'); ?>:</label>
    <select id="comp" onchange="if (this.value){location.href=this.value;}">
      <?php foreach ($options as $option): ?>
        <?php print $option; ?>
      <?php endforeach; ?>
    </select>
</div>
<div class="rankings-wrapper">
    <div class="ranking-wrapper header">
        <div class="user"><?php print t('Username'); ?></div>
        <div class="exact"><?php print t('Exact'); ?></div>
        <div class="points"><?php print t('Points'); ?></div>
    </div>
  <?php $count = 1; ?>
  <?php foreach ($rows as $row): ?>
      <div class="ranking-wrapper pos-<?php print $count; ?>">
          <div class="user"><?php print $count . '. ' . $row->name; ?></div>
          <div class="exact"><?php print $row->exact; ?></div>
          <div class="points"><?php print $row->points; ?></div>
      </div>
    <?php $count++; ?>
  <?php endforeach; ?>
</div>