<script type="text/javascript">
  function generateRandom(name) {
    document.getElementsByName(name)[0].value = Math.floor(Math.random() * 4);
  }
</script>
<?php $params = drupal_get_query_parameters(); ?>
<div class="bets-header">
  <div class="bets-header-left">
    <a href="<?php print url(current_path(), ['absolute' => TRUE, 'query' => array_diff_key(drupal_get_query_parameters(), ['finished' => TRUE])]); ?>" class="<?php !isset($params['finished']) ? print 'active' : NULL; ?>">
      <?php print t('Upcoming games') ?>
    </a>
    <a href="<?php print url(current_path(), ['absolute' => TRUE, 'query' => drupal_get_query_parameters() + ['finished' => TRUE]]); ?>" class="<?php isset($params['finished']) ? print 'active' : NULL; ?>">
      <?php print t('Finished games') ?>
    </a>
  </div>
  <div class="bets-header-right">
    <label for="comp"><?php print t('Competition'); ?>:</label>
    <select id="comp" onchange="if (this.value){location.href=this.value;}">
      <?php foreach ($options as $option): ?>
      <?php print $option; ?>
      <?php endforeach; ?>
    </select>
  </div>
</div>
<div class="bets-wrapper">
  <?php foreach ($rows as $key => $temp_row): ?>
  <?php if (is_array($temp_row)): ?>
    <h2><?php print t('Matchday') . ' ' . $key; ?></h2>
    <?php $temp_rows = $temp_row; ?>
  <?php else: ?>
    <?php $temp_rows = []; ?>
    <?php $temp_rows[] = $temp_row; ?>
  <?php endif; ?>
  <?php foreach ($temp_rows as $row): ?>
  <div class="bet-wrapper">
    <div class="date"><?php print _itr_bet_convert_timezone($row->date); ?></div>
    <div class="home <?php $row->score_home > $row->score_away ? print 'win' : NULL; ?>">
      <div class="bet-stuff-wrapper">
        <div class="team"><?php print _itr_bet_get_term_name($row->home); ?></div>
        <div class="score"><?php print ($row->score_home != '' ? $row->score_home : '-'); ?></div>
      </div>
    </div>
    <div class="away <?php $row->score_home < $row->score_away ? print 'win' : NULL; ?>">
      <div class="bet-stuff-wrapper">
        <div class="score"><?php print ($row->score_away != '' ? $row->score_away : '-'); ?></div>
        <div class="team"><?php print _itr_bet_get_term_name($row->away); ?></div>
      </div>
    </div>
    <div class="bet">
      <p><?php print t('Your bet') ?></p>
      <?php if (!isset($params['finished']) && $row->score_home == '' && $row->score_away == ''): ?>
        <div class="form-wrapper">
          <input class="input-bet" type="number" min="0" step="1" name="<?php print $row->nid; ?>_home" value="<?php print $row->user_home; ?>"/>
          <input class="input-bet" type="number" min="0" step="1" name="<?php print $row->nid; ?>_away" value="<?php print $row->user_away; ?>"/>
        </div>
        <a href="#" onclick="generateRandom('<?php print $row->nid; ?>_home'); generateRandom('<?php print $row->nid; ?>_away'); return false;"><?php print t('Random'); ?></a>
      <?php else: ?>
        <?php // Set class for correct/exact ?>
        <?php $class =
          ($row->score_home != '' && $row->score_away != '') &&
          (($row->score_home == $row->score_away && $row->user_home == $row->user_away) ||
            ($row->score_home < $row->score_away && $row->user_home < $row->user_away) ||
            ($row->score_home > $row->score_away && $row->user_home > $row->user_away)) ? 'correct' : ''; ?>
        <?php $class = $row->score_home == $row->user_home && $row->score_away == $row->user_away ? 'exact' : $class; ?>
        <?php $class = $row->score_home != '' && $row->score_away != '' && empty($class) ? 'wrong' : $class; ?>
        <div class="form-wrapper <?php print $class; ?>">
          <span><?php $row->user_home == '' ? print '/' : print $row->user_home; ?></span><span>&nbsp;-&nbsp;</span><span><?php $row->user_away == '' ? print '/' : print $row->user_away; ?></span>
        </div>
      <?php endif; ?>
    </div>
    <div class="view"><a href="<?php print url(current_path(), ['absolute' => TRUE, 'query' => ['match' => $row->nid]]); ?>"><?php print t('View bets'); ?></a></div>
  </div>
  <?php endforeach; ?>
  <?php endforeach; ?>
</div>
<?php if (!isset($params['finished'])): ?>
<div class="bets-footer">
  <input id="itr-bet-save" type="submit" value="<?php print t('Save'); ?>"/>
</div>
<?php endif; ?>