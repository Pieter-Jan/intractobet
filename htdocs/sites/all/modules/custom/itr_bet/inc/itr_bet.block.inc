<?php

/**
 * Build main block
 *
 * @return string
 * @throws \Exception
 */
function _itr_bet_main_block() {
  // Get all competition
  $comps = _itr_bet_get_competitions();

  // Check selected parameters
  $params = drupal_get_query_parameters();
  if (isset($params['comp']) && is_numeric($params['comp'])) {
    $comp = $params['comp'];
  } else {
    // No competitions set, first as default
    $comp = key($comps);
  }

  // Get all competition as options
  $options = _itr_bet_build_options($comps, $comp);

  if (isset($params['match']) && is_numeric($params['match'])) {
    // Show one match
    $match = _itr_bet_get_bets(FALSE, $params['match']);
    drupal_add_css(drupal_get_path('module', 'itr_bet') . '/css/itr-bet.detail.css');
    return theme('itr-bet_detail', ['match' => $match[$params['match']], 'rows' => _itr_bet_get_user_bets($params['match'])]);
  }

  // Show all
  if (isset($params['finished'])) {
    $bets = _itr_bet_get_bets($comp, FALSE, FALSE);
  } else {
    $bets = _itr_bet_get_bets($comp);
  }

  drupal_add_css(drupal_get_path('module', 'itr_bet') . '/css/itr-bet.main.css');
  drupal_add_js(drupal_get_path('module', 'itr_bet') . '/js/itr-bet.main.js', ['type' => 'file']);

  $mobile = new Mobile_Detect();
  if ($mobile->isMobile() || $mobile->isTablet()) {
    drupal_add_css(drupal_get_path('module', 'itr_bet') . '/css/itr-bet.main.mobile.css');
    return theme('itr-bet_main_mobile', ['rows' => $bets, 'options' => $options]);
  }

  return theme('itr-bet_main', ['rows' => $bets, 'options' => $options]);
}

/**
 * Build ranking block
 *
 * @return string
 * @throws \Exception
 */
function _itr_bet_ranking_block() {
  // Get all competition
  $comps = _itr_bet_get_competitions();

  // Check selected parameters
  $params = drupal_get_query_parameters();
  if (isset($params['comp']) && is_numeric($params['comp'])) {
    $comp = $params['comp'];
  } else {
    // No competitions set, first as default
    $comp = key($comps);
  }

  // Get all competition as options
  $options = _itr_bet_build_options($comps, $comp);

  drupal_add_css(drupal_get_path('module', 'itr_bet') . '/css/itr-bet.ranking.css');

  $mobile = new Mobile_Detect();
//  if ($mobile->isMobile() || $mobile->isTablet()) {
    drupal_add_css(drupal_get_path('module', 'itr_bet') . '/css/itr-bet.ranking.mobile.css');
//  }

  return theme('itr-bet_ranking', ['rows' => _itr_bet_get_ranking($comp), 'options' => $options]);
}