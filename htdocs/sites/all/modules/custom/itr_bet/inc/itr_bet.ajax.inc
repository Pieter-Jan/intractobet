<?php

/**
 * Handle input of bets
 */
function itr_bet_handle_bets() {
  if (!isset($_POST['bets'])) {
    // No bets, should not happen
    watchdog('itr_bet_error', 'Empty post');
    print 'error 1';
    drupal_exit();
  }

  global $user;
  if (!$user->uid) {
    // No user, cheater!
    watchdog('itr_bet_error', 'No user is set');
    print 'error 2';
    drupal_exit();
  }

  $bets = json_decode($_POST['bets']);
  foreach ($bets as $bet) {
    if ($bet->value == '') {
      // Empty bet, do not insert
      continue;
    }

    $keys = explode('_', $bet->name);
    if (count($keys) != 2) {
      // Invalid key, cheater
      watchdog('itr_bet_error', 'Invalid form for user '.$user->uid);
      print 'error 3';
      drupal_exit();
    }
    // [0] = nid
    // [1] = home/away

    // Check if node is published
    if (!_itr_bet_node_is_published($keys[0])) {
      watchdog('itr_bet_error', 'Invalid or unpublished node '.$keys[0].' for user '.$user->uid);
      print 'error 4';
      drupal_exit();
    }

    // Check if form is home or away
    if (!in_array($keys[1], ['home', 'away'], FALSE) ){
      watchdog('itr_bet_error', 'Invalid key '.$keys[1].' for user '.$user->uid);
      print 'error 5';
      drupal_exit();
    }

    // Check if value is a number
    if (!is_numeric($bet->value)) {
      watchdog('itr_bet_error', 'Invalid value '.$bet->value.' for user '.$user->uid);
      print 'error 6';
      drupal_exit();
    }

    try {
      // Merge into the database
      db_merge('itr_bet')
        ->key(['nid' => $keys[0], 'uid' => $user->uid])
        ->fields(
          [
            'nid' => $keys[0],
            'uid' => $user->uid,
            $keys[1] => $bet->value,
          ]
        )
        ->execute();
    } catch (Exception $e) {
      // Error, return false
      watchdog('itr_bet_error', 'SQL: '.$e->getMessage());
      print 'error 7';
      drupal_exit();
    }
  }

  print '';
  drupal_exit();
}

/**
 * Return the teams for the given competition
 */
function itr_bet_get_options() {
  // Get selected competition
  $comp = isset($_POST['comp']) ? $_POST['comp'] : FALSE;

  if (!is_numeric($comp)) {
    // Invalid or empty
    print json_encode([]);
    drupal_exit();
  }

  // Return teams
  print json_encode(_itr_bet_get_teams_from_competition($comp));
  drupal_exit();
}