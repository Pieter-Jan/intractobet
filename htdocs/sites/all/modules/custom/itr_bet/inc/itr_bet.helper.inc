<?php

/**
 * Generate title based on the selected teams
 *
 * @param $values
 *
 * @return string
 */
function _itr_bet_generate_bet_title($values) {
  if (empty($values['field_bet_home'][LANGUAGE_NONE][0]['target_id']) || empty($values['field_bet_away'][LANGUAGE_NONE][0]['target_id'])) {
    // Empty values, return empty string
    return '';
  }

  // Return title
  return _itr_bet_get_term_name($values['field_bet_home'][LANGUAGE_NONE][0]['target_id']) . ' - ' . _itr_bet_get_term_name($values['field_bet_away'][LANGUAGE_NONE][0]['target_id']);
}

/**
 * Return the name of the term with given term id
 *
 * @param $tid
 *
 * @return string
 */
function _itr_bet_get_term_name($tid) {
  $sql = "SELECT t.name
          FROM {taxonomy_term_data} t
          WHERE t.tid = :tid;";
  $result = db_query($sql, [':tid' => $tid])
    ->fetchAssoc();

  if (isset($result['name'])) {
    return $result['name'];
  }

  return '';
}

/**
 * Return the icon of the term with given term id
 *
 * @param $tid
 *
 * @return bool|string
 */
function _itr_get_term_icon($tid) {
  $sql = "SELECT f.uri
          FROM {field_data_field_icon} i
          INNER JOIN file_managed f ON f.fid = i.field_icon_fid
          WHERE i.entity_id = :tid;";
  $result = db_query($sql, [':tid' => $tid])
    ->fetchAssoc();

  if (isset($result['uri'])) {
    return image_style_url('icon', $result['uri']);
  }

  return FALSE;
}

/**
 * Get all or one match/bets
 *
 * @param $comp
 * @param $match
 * @param $active
 *
 * @return mixed
 */
function _itr_bet_get_bets($comp = 0, $match = 0, $active = TRUE) {
  global $user;

  // Default query
  $sql = "SELECT n.nid, h.field_bet_home_target_id AS home, a.field_bet_away_target_id AS away,
            c.field_bet_competitie_target_id AS comp, m.field_bet_match_day_value AS matchday,
            d.field_bet_date_value AS date, sh.field_bet_score_home_value AS score_home,
            sa.field_bet_score_away_value AS score_away, b.home AS user_home, b.away AS user_away
          FROM {node} n
          INNER JOIN {field_data_field_bet_home} h ON h.entity_id = n.nid
          INNER JOIN {field_data_field_bet_away} a ON a.entity_id = n.nid
          INNER JOIN {field_data_field_bet_competitie} c ON c.entity_id = n.nid
          LEFT JOIN {field_data_field_bet_match_day} m ON m.entity_id = n.nid
          INNER JOIN {field_data_field_bet_date} d ON d.entity_id = n.nid
          LEFT JOIN {field_data_field_bet_score_home} sh ON sh.entity_id = n.nid
          LEFT JOIN {field_data_field_bet_score_away} sa ON sa.entity_id = n.nid
          LEFT JOIN {itr_bet} b ON b.nid = n.nid AND b.uid = :uid
          WHERE n.type = :type ";
  // Default params
  $params = [
    ':type' => 'bet',
    ':uid' => $user->uid,
  ];

  // Check if a match is requested
  if (!empty($match)) {
    $sql .= "AND n.nid = :nid ";
    $params['nid'] = $match;
  } else {
    // No match requested, add compitions
    $sql .= "AND c.field_bet_competitie_target_id = :comp
             AND n.status = :status ";
    $params[':comp'] = $comp;
    $params[':status'] = $active ? NODE_PUBLISHED : NODE_NOT_PUBLISHED;
  }

  if ($active) {
    $sql .= "ORDER BY d.field_bet_date_value;";
  } else {
    $sql .= "ORDER BY d.field_bet_date_value DESC;";
  }

  // Get bets
  $results = db_query($sql, $params)
    ->fetchAllAssoc('nid');

  // If matchday(s) is/are set, use this order
  $results = _itr_bet_order_matchdays($results);

  return $results;
}

/**
 * Get all active competitions
 *
 * @return mixed
 */
function _itr_bet_get_competitions() {
  $sql = "SELECT DISTINCT t.tid, t.name
          FROM  {node} n
          INNER JOIN {field_data_field_bet_competitie} c ON c.entity_id = n.nid
          INNER JOIN {taxonomy_term_data} t ON t.tid = c.field_bet_competitie_target_id
          WHERE n.type = :type
          ORDER BY t.name;";
  $results = db_query($sql, [':type' => 'bet'])
    ->fetchAllAssoc('tid');

  return $results;
}

/**
 * Build list of options
 *
 * @param $comps
 * @param $default
 *
 * @return array
 */
function _itr_bet_build_options($comps, $default) {
  $options = [];
  foreach ($comps as $c) {
    $options[] = '<option value="'.url(current_path(), ['absolute' => TRUE, 'query' => ['comp' => $c->tid]]).'" '.($c->tid == $default ? 'selected' : NULL).'>'.$c->name.'</option>';
  }

  return $options;
}

/**
 * Check if a nid is published
 *
 * @param $nid
 *
 * @return bool
 */
function _itr_bet_node_is_published($nid) {
  $sql = "SELECT n.status
          FROM {node} n
          WHERE n.nid = :nid
          AND n.type = :type;";
  $result = db_query($sql, [':nid' => $nid, ':type' => 'bet'])
    ->fetchAssoc();

  if (isset($result['status'])) {
    return !empty($result['status']);
  }

  return FALSE;
}

/**
 * Update scores
 *
 * @param $nid
 * @param $home
 * @param $away
 */
function _itr_bet_calculate_scores($nid, $home, $away) {
  // Get all bets from this node
  $sql = "SELECT i.*
          FROM {itr_bet} i
          WHERE i.nid = :nid;";
  $results = db_query($sql, [':nid' => $nid])
    ->fetchAllAssoc('id');

  foreach ($results as $result) {
    // Calculate points
    $points = 0;
    if ($home == $result->home && $away == $result->away) {
      $points = 3;
    } elseif (($home == $away && $result->home == $result->away) ||
              ($home < $away && $result->home < $result->away) ||
              ($home > $away && $result->home > $result->away)) {
      $points = 1;
    }
    // Update points
    db_update('itr_bet')
      ->fields(['points' => $points])
      ->condition('id', $result->id, '=')
      ->execute();
  }
}

/**
 * Get the ranking for the given competition
 *
 * @param $comp
 *
 * @return mixed
 */
function _itr_bet_get_ranking($comp) {
  $sql = "SELECT u.uid, u.name, SUM(b.points) AS points, SUM(IF(b.points = 3, 1, 0)) AS exact
          FROM {node} n
          INNER JOIN {field_data_field_bet_competitie} c ON c.entity_id = n.nid
          INNER JOIN {itr_bet} b ON b.nid = n.nid
          INNER JOIN {users} u ON u.uid = b.uid
          WHERE c.field_bet_competitie_target_id = :comp
          GROUP BY b.uid
          ORDER BY SUM(b.points) DESC, SUM(IF(b.points = 3, 1, 0)) DESC, u.name;";
  $results = db_query($sql, [':comp' => $comp])
    ->fetchAllAssoc('uid');

  return $results;
}

/**
 * Get all bets from given nid
 *
 * @param $nid
 *
 * @return mixed
 */
function _itr_bet_get_user_bets($nid) {
  $sql = "SELECT b.id, u.name, b.home, b.away
          FROM itr_bet b
          INNER JOIN users u ON u.uid = b.uid
          WHERE b.nid = :nid
          ORDER BY b.home, b.away, u.name;";
  $results = db_query($sql, [':nid' => $nid])
    ->fetchAllAssoc('id');

  return $results;
}

/**
 * Convert timezone from the database to that of the server
 *
 * @param $date
 * @param $fomat
 *
 * @return string
 */
function _itr_bet_convert_timezone($date, $format = 'D d/m H:i') {
  // Date is saved as UTC
  $database_timezone = new DateTimeZone('UTC');
  // Need to be converted to server timezone
  $local_timezone = new DateTimeZone(date_default_timezone_get());

  $datetime = new DateTime($date, $database_timezone);
  $datetime->setTimezone($local_timezone);

  return $datetime->format($format);
}

/**
 * Get all teams from the given competition
 *
 * @param $comp
 *
 * @return mixed
 */
function _itr_bet_get_teams_from_competition($comp) {
  $sql = "SELECT d.tid, d.name
          FROM {field_data_field_competitie} c
          INNER JOIN taxonomy_term_data d ON d.tid = c.entity_id
          WHERE c.field_competitie_tid = :comp
          ORDER BY d.name;";
  $results = db_query($sql, [':comp' => $comp])
    ->fetchAllAssoc('tid');

  return $results;
}

function _itr_bet_order_matchdays($rows) {
  $return = [];

  foreach ($rows as $row) {
    if (!$row->matchday) {
      continue;
    }
    $return[$row->matchday][] = $row;
  }

  if (!empty($return)) {
    ksort($return);
    return $return;
  }
  return $rows;
}