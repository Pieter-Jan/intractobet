(function ($) {

  /**
   * Enable both team selects
   */
  function enableOptions() {
    $('#edit-field-bet-home-und').removeAttr('disabled');
    $('#edit-field-bet-away-und').removeAttr('disabled');
  }

  /**
   * Disable and empty both team selects
   */
  function disableOptions() {
    $('#edit-field-bet-home-und').empty().attr('disabled','disabled');
    $('#edit-field-bet-away-und').empty().attr('disabled','disabled');
  }

  /**
   * Generate the options for the teams with the selected competition
   */
  function getOptions() {
    // Disable and clear
    disableOptions();

    // Make request
    $.ajax({
      url: '/ajax/admin/itr-bet/options',
      type: 'POST',
      data: { 'comp': $('#edit-field-bet-competitie-und').val() },
      success: function(result) {
        // Parse results
        var results = $.parseJSON(result);
        // Loop results
        $.each(results, function(index, value) {
          // Add result to both selects
          $('#edit-field-bet-home-und').append($("<option></option>").attr('value', value.tid).text(value.name));
          $('#edit-field-bet-away-und').append($("<option></option>").attr('value', value.tid).text(value.name));
        });
      },
    });

    // Enable
    enableOptions();
  }

  $('document').ready(function() {
    // Update when the competition changes
    $('#edit-field-bet-competitie-und').change(function() {
      getOptions();
    });
  });

})(jQuery);