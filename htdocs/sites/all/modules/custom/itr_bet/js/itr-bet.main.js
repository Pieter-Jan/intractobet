(function ($) {

  function set_loading() {
    $('.input-bet').each(function(index, value) {
      $(value).prop('disabled', true);
    });
    $('#itr-bet-save').val(Drupal.t('Saving'));
  }

  function remove_loading() {
    $('.input-bet').each(function(index, value) {
      $(value).prop('disabled', false);
    });
    $('#itr-bet-save').val(Drupal.t('Bets saved!'));
    setTimeout(function() {
      $('#itr-bet-save').val(Drupal.t('Save'));
    }, 2000);
  }

  function save_bets() {
    // Set loading
    set_loading();

    // Get all inputs
    var inputs = $('.input-bet');

    var bets = [];
    $.each(inputs, function(index, value) {
      // Get data
      var bet = new Object;
      bet.name = $(value).attr('name');
      bet.value = $(value).val();

      // Add input to list
      bets.push(bet);
    });

    // Bets as JSON
    bets = JSON.stringify(bets);

    // Send list to Drupal
    $.ajax({
      url: '/ajax/itr-bet',
      type: 'POST',
      data: { 'bets': bets },
      success: function(result) {
        if (result != '') {
          alert('Something went wrong, please contact Pieter-Jan if this keeps happening ('+result+').');
          location.reload(true);
        } else {
          // Remove loading
          remove_loading();
        }
      },
    });
  }

  $('document').ready(function() {
    $('#itr-bet-save').on('click', function() {
      save_bets();
    });
  });

})(jQuery);